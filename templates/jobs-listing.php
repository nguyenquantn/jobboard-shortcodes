<?php if ($wp_query->have_posts()) : ?>

    <?php do_action('jobboard_loop_before'); ?>

    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

        <?php jb_get_template_part('content', 'jobs'); ?>

    <?php endwhile; ?>

    <?php do_action('jobboard_loop_after'); ?>
    <?php
    global $wp_query;
    $wp_query = $old_query;
    ?>
<?php else: ?>

    <?php jb_get_template_part('loop/not-found'); ?>

<?php endif; ?>

