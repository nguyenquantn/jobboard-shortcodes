<?php
/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 8/14/2017
 * Time: 3:02 PM
 */
function jb_get_jobs_type() {
	$return = array();
	$terms  = get_terms( array(
		'taxonomy'   => 'jobboard-tax-types',
		'hide_empty' => false,
	) );

	if ( is_array( $terms ) ) {
		foreach ( $terms as $term ) {
			$return[ $term->name ] = $term->slug;
		}
	}

	return $return;
}

function jb_get_jobs_by_type( $job_type = 'full-time' ) {
	$args = array(
		'post_type'      => 'jobboard-post-jobs',
		'post_status'    => 'publish',
		'posts_per_page' => jb_get_option( 'posts-per-page', 12 )
	);

	if ( isset( $job_type ) && ! empty( $job_type ) ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'jobboard-tax-types',
				'field'    => 'slug',
				'terms'    => $job_type,
			)
		);
	}

	$jobs = new WP_Query( $args );


	return $jobs;
}


//if ( function_exists( 'vc_map' ) ) {
//	vc_map( array(
//		"name"     => esc_html__( 'Jobs Listing', "jobboard-shortcodes" ),
//		"base"     => "jobboard-jobs-listing",
//		"icon"     => "cs_icon_for_vc",
//		"category" => esc_html__( 'JobBoard', "jobboard-shortcodes" ),
//		"params"   => array(
//			array(
//				"type"        => "dropdown",
//				"heading"     => esc_html__( "Query Type", "jobboard-shortcodes" ),
//				"param_name"  => "type",
//				"admin_label" => true,
//				"value"       => jb_get_jobs_type()
//			)
//		)
//	) );
//}

