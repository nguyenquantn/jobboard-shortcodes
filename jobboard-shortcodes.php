<?php
/**
 * Plugin Name: JobBoard ShortCodes
 * Plugin URI: http://fsflex.com/
 * Description: JobBoard ShortCodes.
 * Version: 1.0.0
 * Author: Quan
 * Author URI: http://fsflex.com/
 * License: GPLv2 or later
 * Text Domain: jobboard-shortcodes
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

// Don't duplicate me!
if ( ! class_exists( 'JB_Shortcodes' ) ) {
	class JB_Shortcodes {
		public static $instance = null;

		public $file;
		public $basename;
		public $plugin_directory;
		public $plugin_directory_uri;

		public $package;
		public $form;

		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new JB_Shortcodes();
				self::$instance->setup_globals();

				if ( ! function_exists( 'is_plugin_active' ) ) {
					require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
				}

				if ( is_plugin_active( 'jobboard/jobboard.php' ) ) {
					self::$instance->includes();
					self::$instance->actions();
				}
			}

			return self::$instance;
		}

		private function setup_globals() {
			$this->file                 = __FILE__;
			$this->basename             = plugin_basename( $this->file );
			$this->plugin_directory     = plugin_dir_path( $this->file );
			$this->plugin_directory_uri = plugin_dir_url( $this->file );
		}

		private function includes() {
			include_once $this->plugin_directory . 'inc/functions-core.php';
		}

		private function actions() {
//			add_shortcode( 'jobboard-jobs-listing', array( $this, 'shortcodes_jobs_listing' ) );
			add_shortcode( 'jobboard-jobs-locations', array( $this, 'shortcodes_jobs_locations' ) );
			add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 2 );
		}

		public function shortcodes_jobs_locations( $atts = array(), $content ) {
			$atts = shortcode_atts(
				array(
					'title'       => esc_html__( 'Jobs by Location', "jobboard-shortcodes" ),
					'description' => __( "Description", "jobboard-shortcodes" ),
					'country'     => 'All Locations',
					'count'       => 4,
					'view'        => '',
				), $atts, 'jobboard-jobs-locations' );

			$country = 0;

			if ( $atts['country'] !== 'All Locations' ) {
				$country = get_term_by( 'name', $atts['country'], 'jobboard-tax-locations' );
				$country = $country->term_id;
			}

			$locations = jb_get_locations( $country, $atts['count']);

			$this->get_template( 'locations-listing.php', array(
				'locations'   => $locations,
				'title'       => $atts['title'],
				'description' => $atts['description'],
				'view'        => $atts['view'],
			) );

		}

		function shortcodes_jobs_listing( $atts = array(), $content = '' ) {
			$atts = shortcode_atts(
				array(
					'job_type' => null
				), $atts, 'jobboard-jobs-listing' );
			global $wp_query;
			$old_query = $wp_query;
			$wp_query  = jb_get_jobs_by_type( $atts['job_type'] );
			$this->get_template( 'jobs-listing.php', array( 'wp_query' => $wp_query, 'old_query' => $old_query ) );
		}

		public function plugin_row_meta( $plugin_meta, $plugin_file ) {
			if ( $plugin_file !== plugin_basename( __FILE__ ) ) {
				return $plugin_meta;
			}

			$plugin_meta[] = '<a href="https://github.com/vanquan805">' . esc_html__( 'GitHub', 'jobboard-import' ) . '</a>';
			$plugin_meta[] = '<a href="http://fsflex.com/support/" title="' . esc_html__( 'Support forum.', 'jobboard-import' ) . '">' . esc_html__( 'Support', 'jobboard-import' ) . '</a>';
			$plugin_meta[] = '<a href="mailto:vanquan805@gmail.com" title="' . esc_html__( 'Send a email to Dev team.', 'jobboard-import' ) . '">' . esc_html__( 'Contact', 'jobboard-import' ) . '</a>';

			return $plugin_meta;

		}


		function get_template( $template_name, $args = array() ) {
			jb_get_template( $template_name, $args, JB()->template_path() . 'add-ons/shortcodes/', $this->plugin_directory . 'templates/' );
		}
	}
}

function jb_shortcodes() {
	return JB_Shortcodes::instance();
}

$GLOBALS['jobboard_shortcodes'] = jb_shortcodes();